import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import {sG} from '../../components/general/styles';
import GameIem from '../../components/games/gameItem';

export default class GamesList extends Component {
  

  handlePressDetails = () => {
      this.props.navigation.navigate('GamesDetails')
  }
  
  render() {
    return (
        <View style={[ sG.container, sG.bg_light]}>
            <View style={[ stylesLocal.h_header, sG.w_100, sG.brounded_bottom, sG.bg_primary]}></View>
            
            <View style={[ sG.h_15, sG.w_100, sG.ai_center, sG.jc_center]}>
              <View style={[ sG.h_70, sG.w_80, sG.ai_center, sG.jc_center]}>
                <View style={[sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
                  <Text style={[sG.h6, sG.bold, sG.text_primary]}>Find here all available games</Text>
                  <Text style={[sG.h8, sG.text_gray_light]}>Encuentra aqui todos los juegos disponibles</Text>
                </View>
              </View>
            </View>

            
        <ScrollView style={[ sG.bg_light]}>
          <View style={[ sG.row_20]}>
            <TouchableOpacity style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressDetails()}>
              <GameIem/>
            </TouchableOpacity>
          </View>
          <View style={[ sG.row_20]}>
            <TouchableOpacity style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressDetails()}>
              <GameIem/>
            </TouchableOpacity>
          </View>
          <View style={[ sG.row_20]}>
            <TouchableOpacity style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressDetails()}>
              <GameIem/>
            </TouchableOpacity>
          </View>
          <View style={[ sG.row_20]}>
            <TouchableOpacity style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressDetails()}>
              <GameIem/>
            </TouchableOpacity>
          </View>
          <View style={[ sG.row_20]}>
            <TouchableOpacity style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressDetails()}>
              <GameIem/>
            </TouchableOpacity>
          </View>
        </ScrollView>

        <View style={[ sG.h_30, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
            <View style={[ sG.h_100, sG.w_30, sG.ai_center, sG.jc_center]}>
              <Image style={[ sG.w_60, sG.h_70, sG.brounded]} resizeMode='cover' source={require('../../../../assets/help1.png')}/>
            </View>
            <View style={[ sG.h_100, sG.w_70, sG.ai_center, sG.jc_center]}>
              <View style={[ sG.h_50, sG.w_90, sG.ai_center, sG.jc_center]}>
                <View style={[ sG.h_80, sG.w_100, sG.brounded, sG.card_shadow, sG.ai_center, sG.jc_center, sG.bg_white]}>
                  <Text style={[sG.h8, sG.text_center, sG.ai_center, sG.jc_center, sG.text_black]}>Deslízate por el menú y selecciona un juego.
                  </Text>
                </View>
              </View>
              <View style={[ sG.h_50, sG.w_90, sG.ai_center, sG.jc_center]}>
                <View style={[ sG.h_80, sG.w_100, sG.brounded, sG.card_shadow, sG.ai_center, sG.jc_center, sG.bg_white]}>
                  <Text style={[sG.h8, sG.text_center, sG.ai_center, sG.jc_center, sG.text_black]}>Scroll through the menu and select a game.
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
    );
  }
}

const stylesLocal = StyleSheet.create({
  h_header:{ 
      height:'12%'
    },
  w_color:{ 
    width:'2%',
  },
  w_details:{ 
    width:'78%'
  },
  border_gray:{ 
    borderRightColor: '#a0a0a0',
    borderRightWidth: 1
  },
  image_config:{
    flex:1,
  }
});