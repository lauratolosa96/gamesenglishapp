import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import {sG} from '../../components/general/styles';

export default class ExamItem extends Component {
  

  handlePress = () => {
      this.props.navigation.navigate('Home')
  }
  
  render() {
    return (
        <View style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]}>
            <View style={[ sG.h_95, sG.w_90, sG.chrow, sG.ai_center, sG.jc_center]}>
              <View style={[ sG.h_100, sG.w_100, sG.chrow, sG.brounded, sG.ai_center, sG.bg_white]}>
                  <View style={[ sG.h_100, stylesLocal.w_color, sG.ai_center, sG.jc_center, sG.brounded, sG.bg_primary]}>
                  </View>
                  <View style={[sG.h_70, sG.w_15, sG.ai_center, sG.jc_center]}>                    
                    <View style={[sG.h_80, sG.w_70, sG.ai_center, sG.jc_center]}>
                      <Image style={[ stylesLocal.image_config]} resizeMode='contain' source={require("../../../../assets/exam.png")} />
                    </View>
                  </View>
                  <View style={[sG.h_100, stylesLocal.w_details, sG.jc_center]}>
                    <Text style={[sG.h7,sG.bold, sG.text_black]}>Nombre en inglés del examen</Text>
                    <Text style={[sG.h8, sG.text_gray_light]}>Nombre en español del examen</Text>
                  </View>
                  <View style={[sG.h_100, sG.w_5, sG.jc_center, sG.ai_center]}>
                    <Text style={[sG.h7, sG.text_primary]}>></Text>
                  </View>
              </View>
            </View>
        </View>
    );
  }
}

const stylesLocal = StyleSheet.create({
  h_header:{ 
      height:'12%'
    },
  w_color:{ 
    width:'2%',
  },
  w_details:{ 
    width:'78%'
  },
  border_gray:{ 
    borderRightColor: '#a0a0a0',
    borderRightWidth: 1
  },
  image_config:{
    flex:1,
  }
});