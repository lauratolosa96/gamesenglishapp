import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class PasswordOne extends Component {
  
  handlePress = () => {
      this.props.navigation.navigate('PasswordTwo')
  }
  
  render() {
    return (
      <View style={[ sG.container, sG.bg_light]}>

        <View style={[ sG.h_25, sG.w_100, sG.ai_center, sG.jc_center]}>
          <Text style={[sG.h8, sG.text_center, sG.text_gray_light]}>ingresa tu cédula y Correo electrónico y enviaremos{'\n'} un código de verificación para que puedas{'\n'}cambiar la contraseña de tu cuenta.
          </Text>
        </View>

        <View style={[sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
          <View style={[sG.h_85, sG.w_90, sG.bg_white, sG.brounded, sG.ai_center, sG.jc_center]}>
            <View style={[sG.h_30, sG.w_85]}>
              <Text style={[sG.h8, sG.text_primary]}>Cédula de Ciudadanía</Text>
            </View>
            <TextInput style={[sG.h_65, sG.w_85, sG.h7]} placeholder='Cédula de Ciudadanía' placeholderTextColor = '#BDBDBD'>
            </TextInput>
          </View>
        </View>

        <View style={[sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
          <View style={[sG.h_85, sG.w_90, sG.bg_white, sG.brounded, sG.ai_center, sG.jc_center]}>
            <View style={[sG.h_30, sG.w_85]}>
              <Text style={[sG.h8, sG.text_primary]}>Correo electrónico</Text>
            </View>
            <TextInput style={[sG.h_65, sG.w_85, sG.h7]} placeholder='xxxxxxxxxx@gmail.com' placeholderTextColor = '#BDBDBD'>
            </TextInput>
          </View>
        </View>

        <View style={[sG.h_55, sG.w_100, sG.ai_center, sG.jc_end]}>
          <TouchableOpacity style={[sG.h_15, sG.w_100, sG.ai_center, sG.jc_center, sG.bg_primary]} onPress={() =>  this.handlePress()}>
            <Text style={[sG.h7, sG.text_white, sG.bold]}>Guardar Cambios</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}