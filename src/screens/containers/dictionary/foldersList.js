import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, ScrollView } from 'react-native';
import {sG} from '../../components/general/styles';
import { List } from '../../components/apis/apisDictionary/apiGeneralList';
import { MaterialCommunityIcons, MaterialIcons, Entypo } from '@expo/vector-icons';
import CardItem from '../../components/dictionary/cardItem';

export default class FolderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: List,
      id_selected: 0,
    };
  }

  handlePressDetails (id, nombre){
    this.props.navigation.navigate('FolderDetails', {id: id, title: nombre})
    //console.log(this.state.dataSource[4].nombre)
  }

  handlePressBack = () => {
    this.props.navigation.navigate('Home')
    }
  
  render() {
    return (
        <View style={[ sG.container, sG.bg_light]}>
          <View style={[ sG.h_15, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
            <View style={[ sG.h_100, sG.w_80, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_10, sG.w_90, sG.jc_center]}>
                <Text style={[sG.h5, sG.bold, sG.text_primary]}>Dictionary</Text>
              </View>
            </View>
            <View style={[ sG.h_100, sG.w_20, sG.ai_center, sG.jc_center]}>
              <TouchableOpacity style={[ sG.h_70, sG.w_70, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressBack()}>
                <MaterialCommunityIcons name='window-close' style={[sG.size_icon]} color='#d81b60' /> 
              </TouchableOpacity>
            </View>
          </View>

          <ScrollView>
            {this.state.dataSource.map((item, key) => (
              <View style={[ sG.row_30, sG.ai_center, sG.jc_cente]}>
                <TouchableOpacity style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressDetails(item.id, item.nombre)}>
                  <CardItem title = {item.nombre}/>
                </TouchableOpacity>              
              </View>
            ))}
          </ScrollView>

          <View style={[ sG.h_30, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
            <View style={[ sG.h_100, sG.w_30, sG.ai_center, sG.jc_center]}>
              <Image style={[ sG.w_60, sG.h_70, sG.brounded]} resizeMode='cover' source={require('../../../../assets/help1.png')}/>
            </View>
            <View style={[ sG.h_100, sG.w_70, sG.ai_center, sG.jc_center]}>
              <View style={[ sG.h_50, sG.w_90, sG.ai_center, sG.jc_center]}>
                <View style={[ sG.h_80, sG.w_100, sG.brounded, sG.card_shadow, sG.ai_center, sG.jc_center, sG.bg_white]}>
                  <Text style={[sG.h8, sG.text_center, sG.ai_center, sG.jc_center, sG.text_black]}>Deslízate por el menú y selecciona el contenido que deseas ver.
                  </Text>
                </View>
              </View>
              <View style={[ sG.h_50, sG.w_90, sG.ai_center, sG.jc_center]}>
                <View style={[ sG.h_80, sG.w_100, sG.brounded, sG.card_shadow, sG.ai_center, sG.jc_center, sG.bg_white]}>
                  <Text style={[sG.h8, sG.text_center, sG.ai_center, sG.jc_center, sG.text_black]}>Scroll through the menu and select the content you want to see.
                  </Text>
                </View>
              </View>
            </View>
          </View>
      </View>
    );
  }
}