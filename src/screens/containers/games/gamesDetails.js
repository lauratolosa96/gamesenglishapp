import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialIcons, FontAwesome, Feather,AntDesign } from '@expo/vector-icons';

export default class GamesDetails extends Component {

  render() {
    return (
      <View style={[ sG.container, sG.bg_light]}>
        <View style={[ sG.h_25, sG.w_100, sG.chrow, sG.brounded_bottom, sG.jc_center, sG.bg_primary]}>
            <View style={[ sG.h_100, sG.w_60]}>
                <ImageBackground style={[ sG.w_100, sG.h_100, sG.ai_center, sG.jc_center]} resizeMode='cover' source={require('../../../../assets/header_logo_1.png')}/>
            </View>
        </View >

        <View style={[ sG.w_100, sG.h_10, sG.ai_center, sG.jc_center]}>
            <View style={[sG.h_100, sG.w_100, sG.ai_center, sG.jc_center, sG.m_Img_negative]}>
                <Image style={[ sG.container]} resizeMode='contain' source={require("../../../../assets/play.png")} />
            </View>
        </View>

        <View style={[ sG.w_100, sG.h_10, sG.ai_center, sG.jc_center]}>
            <View style={[ sG.w_100, sG.h_100, sG.ai_center, sG.jc_center, sG.m_Img_negative]}>
                <Text style={[sG.h6, sG.bold, sG.text_primary]}>Nombre en inglés del juego</Text>
                <Text style={[sG.h8, sG.text_gray_light]}>Nombre en español del juego</Text>
            </View>  
        </View>

        <ScrollView style={[sG.bg_light]}>
            <View style={[ sG.row_30, sG.chrow, sG.ai_center, sG.jc_center]}>
                <Text style={[sG.h8, sG.text_gray_light]}>aqui va el juego</Text>
            </View>
        </ScrollView>
      </View>
    );
  }
}

const stylesLocal = StyleSheet.create({
    borber_bottom:{
      borderBottomColor: '#177a76',
      borderBottomWidth: 1,
    },
});