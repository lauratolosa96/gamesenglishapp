import React, { Component } from "react";
import EStyleSheet from 'react-native-extended-stylesheet';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialIcons, Entypo } from '@expo/vector-icons';


class CardSearch extends Component {
    render() {
        return (
            <View style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_80, sG.w_90, sG.chrow, sG.brounded, sG.ai_center, sG.jc_center, sG.bg_white]}>
                <View style={[sG.h_80, sG.w_30, sG.ai_end, sG.jc_center]}>
                  <Image resizeMode='contain' source={require("../../../../assets/search_icon.png")} style={[sG.h_70, sG.w_65]}/>
                </View>
                <View style={[sG.h_20, sG.w_70, sG.jc_center]}>
                  <Text style={[sG.h8, sG.p_x_sm, sG.bold, sG.text_primary]}>{this.props.title}</Text>
                  <Text style={[sG.h8, sG.p_x_sm, sG.text_gray_light]}>{this.props.simple}</Text>
                  <Text style={[sG.h8, sG.p_x_sm, sG.text_gray_light]}>{this.props.participle}</Text>
                  <Text style={[sG.h8, sG.p_x_sm, sG.text_gray_light]}>{this.props.meaning}</Text>
                </View>
              </View>
            </View>
        );
    }
}

export default CardSearch;