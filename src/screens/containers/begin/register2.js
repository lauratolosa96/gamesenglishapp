import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';

export default class RegisterTwo extends Component {
  

  handlePress = () => {
      this.props.navigation.navigate('Home')
  }
  
  render() {
    return (
      <View style={[ sG.container, sG.bg_light]}>
        <ImageBackground style={[sG.w_100,sG.container, sG.ai_center, sG.jc_center]}
      resizeMode='cover' source={require('../../../../assets/fondo1.png')}>
        <View style={[sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]}>

            <View style={[sG.h_10, sG.w_100, sG.ai_center, sG.jc_end]}>
              <Text style={[sG.h4, sG.bold, sG.text_primary]}>Here you will find...</Text>
            </View>

            <View style={[sG.h_15, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_90, sG.w_30, sG.ai_end, sG.jc_center]}>
                <Image source={require("../../../../assets/photo_icon_clean.png")} style={[sG.h_70, sG.w_65]}/>
              </View>
              <View style={[sG.h_20, sG.w_70, sG.ai_center, sG.jc_center]}>
                <Text style={[sG.h6, sG.bold, sG.text_primary]}>Dictionary</Text>
                <Text style={[sG.h8, sG.text_black]}>Regular, irregular verbs and more...</Text>
              </View>
            </View>
                
            <View style={[sG.h_15, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_90, sG.w_30, sG.ai_end, sG.jc_center]}>
                <Image source={require("../../../../assets/book_audio.png")} style={[sG.h_70, sG.w_65]}/>
              </View>
              <View style={[sG.h_20, sG.w_70, sG.ai_center, sG.jc_center]}>
                <Text style={[sG.h6, sG.bold, sG.text_primary]}>Audiobook</Text>
                <Text style={[sG.h8, sG.text_black]}>Children's stories with audio.</Text>
              </View>
            </View>      

            <View style={[sG.h_15, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_90, sG.w_30, sG.ai_end, sG.jc_center]}>
                <Image source={require("../../../../assets/play.png")} style={[sG.h_70, sG.w_65]}/>
              </View>
              <View style={[sG.h_20, sG.w_70, sG.ai_center, sG.jc_center]}>
                <Text style={[sG.h6, sG.bold, sG.text_primary]}>Games</Text>
                <Text style={[sG.h8, sG.text_black]}>You will find some games to {'\n'} increase your agility.</Text>
              </View>
            </View> 

            <View style={[sG.h_15, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_90, sG.w_30, sG.ai_end, sG.jc_center]}>
                <Image source={require("../../../../assets/exam.png")} style={[sG.h_70, sG.w_65]}/>
              </View>
              <View style={[sG.h_20, sG.w_70, sG.ai_center, sG.jc_center]}>
                <Text style={[sG.h6, sG.bold, sG.text_primary]}>Exams</Text>
                <Text style={[sG.h8, sG.text_black]}>Test your knowledge.</Text>
              </View>
            </View>    
            <View style={[sG.h_30, sG.w_100, sG.ai_center, sG.jc_center]}>
              <TouchableOpacity style={[sG.h_25, sG.w_60, sG.ai_center, sG.jc_center, sG.brounded, sG.bg_primary]} onPress={() =>  this.handlePress()}>
                <Text style={[sG.h5, sG.bold, sG.text_white]}>Start</Text>
              </TouchableOpacity>
            </View>  
          </View>
        </ImageBackground>
      </View>
    );
  }
}