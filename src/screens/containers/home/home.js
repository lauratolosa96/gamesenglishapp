import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialIcons, FontAwesome } from '@expo/vector-icons';
import Publication from '../../components/home/publication';

export default class Home extends Component {

  handlePressDictionary = () => {
    this.props.navigation.navigate('FolderList')
  }

  handlePressGamesList = () => {
    this.props.navigation.navigate('GamesList')
  }

  render() {
    return (
      <View style={[ sG.container, sG.bg_light]}>
        <View style={[ sG.h_30, sG.chrow, sG.brounded_bottom, sG.bg_primary]}>
          <View style={[ sG.w_60, sG.h_100, sG.ai_center, sG.jc_center]}>
            <View style={[ sG.w_90, sG.h_15]}>
              <Text style={[sG.h6,sG.bold, sG.text_white]}>Hi Pepito</Text>
            </View>
            <View style={[ sG.w_90, sG.h_10]}>
              <Text style={[sG.h8, sG.text_white]}>Open the dictionary >>>>>></Text>
            </View>
          </View>
            <View style={[ sG.w_50, sG.h_100, sG.ai_center, sG.jc_center, sG.bg_primary]}>
              <ImageBackground style={[ sG.w_100, sG.h_100, sG.ai_center, sG.jc_center]} resizeMode='cover' source={require('../../../../assets/header_logo_1.png')}>
                <View style={[ sG.w_30, sG.h_30, sG.ai_center, sG.jc_center]}>
                  <TouchableOpacity style={[ sG.w_50, sG.h_100, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressDictionary()}>
                    <FontAwesome name='book' style={[sG.size_icon]} color='#fff' /> 
                  </TouchableOpacity>
                </View>
              </ImageBackground>
            </View>
        </View>

        <View style={[ sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
          <View style={[ sG.h_100, sG.w_95, sG.brounded, sG.card_shadow, sG.ai_center, sG.jc_center, stylesLocal.m_negative, sG.bg_white]}>
            <TouchableOpacity style={[ sG.h_80, sG.w_90, sG.ai_center, sG.chrow, sG.jc_center]} onPress={() =>  this.handlePressDictionary()}>
              <View style={[ sG.h_100, sG.w_70, sG.jc_center]}>
                <View style={[ sG.w_100, sG.h_50]}>
                  <Text style={[sG.h7,sG.bold, sG.text_primary]}>Dictionary</Text>
                </View>
                <View style={[ sG.w_100, sG.h_50]}>
                  <Text style={[sG.h8, sG.text_gray_light]}>Open the dictionary</Text>
                </View>
              </View>
              <View style={[ sG.h_100, sG.w_30]}>
                <View style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]} >
                  <Text style={[sG.h7, sG.bold, sG.text_primary]}>Open ></Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView style={[ sG.bg_light]}>
          <View style={[ sG.row_80, sG.ai_center, sG.jc_center]}>
            <TouchableOpacity style={[ sG.h_95, sG.w_90, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressGamesList()}>
              <Publication/>
            </TouchableOpacity>
          </View>
          <View style={[ sG.row_80, sG.ai_center, sG.jc_center]}>
            <TouchableOpacity style={[ sG.h_95, sG.w_90, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressGamesList()}>
              <Publication/>
            </TouchableOpacity>
          </View>    
          <View style={[ sG.row_80, sG.ai_center, sG.jc_center]}>
            <TouchableOpacity style={[ sG.h_95, sG.w_90, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressGamesList()}>
              <Publication/>
            </TouchableOpacity>
          </View>    
          <View style={[ sG.row_80, sG.ai_center, sG.jc_center]}>
            <TouchableOpacity style={[ sG.h_95, sG.w_90, sG.ai_center, sG.jc_center]} onPress={() =>  this.handlePressGamesList()}>
              <Publication/>
            </TouchableOpacity>
          </View>    
        </ScrollView>
      </View>
    );
  }
}

const stylesLocal = StyleSheet.create({
  m_negative:{ 
      marginTop: -55,
    },
});