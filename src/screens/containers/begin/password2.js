import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class Login extends Component {
  
  handlePress = () => {
      this.props.navigation.navigate('PasswordThree')
  }

  render() {
    return (
      <View style={[ sG.container, sG.bg_light, sG.ai_center, sG.jc_center]}>

        <View style={[ sG.h_20, sG.w_100, sG.ai_center, sG.jc_end]}>
          <Text style={[sG.h8, sG.text_center, sG.text_gray_light]}>A continuación ingresa el código de verificación{'\n'}que enviamos a tu correo electroníco.
          </Text>
        </View>

        <View style={[ sG.h_5, sG.w_100, sG.ai_center, sG.jc_center]}></View>

        <View style={[sG.h_10, sG.w_90, sG.ai_center, sG.jc_center, sG.chrow]}>
          <View style={[sG.h_85, sG.w_25, sG.ai_center, sG.jc_center]}>
            <TextInput style={[sG.h_85, sG.w_60, sG.h2, sG.text_center, sG.bg_white]} placeholder='X' placeholderTextColor = '#a0a0a0'>
            </TextInput>
          </View>
          <View style={[sG.h_85, sG.w_25, sG.ai_center, sG.jc_center]}>
            <TextInput style={[sG.h_85, sG.w_60, sG.h2, sG.text_center, sG.bg_white]} placeholder='X' placeholderTextColor = '#a0a0a0'>
            </TextInput>
          </View>
          <View style={[sG.h_85, sG.w_25, sG.ai_center, sG.jc_center]}>
            <TextInput style={[sG.h_85, sG.w_60, sG.h2, sG.text_center, sG.bg_white]} placeholder='X' placeholderTextColor = '#a0a0a0'>
            </TextInput>
          </View>
          <View style={[sG.h_85, sG.w_25, sG.ai_center, sG.jc_center]}>
            <TextInput style={[sG.h_85, sG.w_60, sG.h2, sG.text_center, sG.bg_white]} placeholder='X' placeholderTextColor = '#a0a0a0'>
            </TextInput>
          </View>
        </View>

        <View style={[stylesLocal.bc_combo_botton, sG.w_100]}></View>

        <View style={[sG.h_45, sG.w_100, sG.ai_center, sG.jc_end]}>
          <TouchableOpacity style={[sG.h_15, sG.w_60, sG.bg_light, sG.ai_center, sG.jc_center, sG.bg_light]}>
          <Text style={[sG.h8,sG.bold, sG.underline, sG.text_primary]}>No recibi mi código de verificación</Text>
          </TouchableOpacity>
        </View>

        <View style={[stylesLocal.bc_botton, sG.w_100, sG.ai_center, sG.jc_center]}>
          <TouchableOpacity style={[sG.h_100, sG.w_100, sG.bg_light, sG.ai_center, sG.jc_center, sG.bg_primary]} onPress={() =>  this.handlePress()}>
            
            <Text style={[sG.h7, sG.text_white, sG.bold]}>Verificar</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const stylesLocal = StyleSheet.create({
  bc_botton:{
    height:'7%'
  },
  bc_combo_botton:{
    height:'13%'
  },
});
