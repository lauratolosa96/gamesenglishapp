import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class Login extends Component {
  constructor(props) { 
    super(props);
    this.state = {
      animating: false,
      align: 'center',
      alignsecond: false,
      checked: false,
      username: '',
      password: '',
      secureTextEntry: true ,
      iconName: "eye-off",
    };
    setTimeout(
      () =>
        this.setState({ align: 'flex-start' }, function() {
          this.setState({
            alignsecond: true,
          });
        }),
      3000
    );
  }


  onIconPress = () => {
    let iconName = (this.state.secureTextEntry) ? "eye" : "eye-off";

    this.setState({
        secureTextEntry: !this.state.secureTextEntry,
        iconName : iconName,
        checked: !this.state.checked
    });
  }

  handlePress = () => {
      this.props.navigation.navigate('RegisterOne')
  }

  handlePressPassword = () => {
      this.props.navigation.navigate('PasswordOne')
  }
          
  render() {
    return (
      <View style={[ sG.container, sG.bg_light]}>
        {this.state.alignsecond?null:(
        <ImageBackground style={[sG.w_100,sG.container, sG.ai_center, sG.jc_center]} resizeMode='cover' source={require('../../../../assets/fondoanimate.jpg')}>
            <View style={[sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]}>
            <View style={[sG.h_30, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
              <Text style={[sG.h4, sG.bold, sG.text_primary]}>Credits</Text>
            </View>
            <View style={[sG.h_15, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_90, sG.w_30, sG.ai_end, sG.jc_center]}>
                <Image source={require("../../../../assets/ruben_profile.png")} style={[sG.h_70, sG.w_60]}/>
              </View>
              <View style={[sG.h_20, sG.w_70, sG.ai_center, sG.jc_center]}>
                <Text style={[sG.h6, sG.bold, sG.text_primary]}>Ruben Dario Parra C.</Text>
                <Text style={[sG.h8, sG.text_black]}>Junior Developer.</Text>
              </View>
            </View>     
            <View style={[sG.h_15, sG.w_100, sG.chrow, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_90, sG.w_30, sG.ai_end, sG.jc_center]}>
                <Image source={require("../../../../assets/laura_profile.png")} style={[sG.h_70, sG.w_60]}/>
              </View>
              <View style={[sG.h_20, sG.w_70, sG.ai_center, sG.jc_center]}>
                <Text style={[sG.h6, sG.bold, sG.text_primary]}>Laura Karina Tolosa C.</Text>
                <Text style={[sG.h8, sG.text_black]}>Junior Developer.</Text>
              </View>
            </View>     
            <View style={[sG.h_40, sG.w_100]}></View>  
          </View>
        </ImageBackground>
        )}

        {!this.state.alignsecond ? null : (

      
        <View style={[ sG.container, sG.bg_light]}>
          <View style={[sG.h_10, sG.w_100]}></View>

          <View style={[sG.h_25, sG.w_100, sG.ai_center, sG.jc_center]}>
            <Image source={require("../../../../assets/fondo_brain.png")} style={[sG.h_90, sG.w_50]}/>
          </View>

          <View style={[sG.h_5, sG.w_100, sG.ai_center, sG.jc_center]}>
            <Text style={[sG.h5, sG.bold, sG.text_primary]}>English Games</Text>
          </View>
          <View style={[sG.h_5, sG.w_100, sG.ai_center, sG.jc_center]}>
            <Text style={[sG.h7, sG.bold, sG.text_primary]}>Welcome!</Text>
          </View>

          <View style={[sG.h_30, sG.w_100, sG.ai_center, sG.jc_center]}>
            <View style={[sG.h_5, sG.w_90, sG.jc_center]}>
              <Text style={[sG.h7, sG.bold, sG.text_primary]}>Write your name:</Text>
            </View>
            <View style={[sG.h_50, sG.w_90, sG.jc_center]}>
              <View style={[sG.h_70, sG.w_100, sG.bg_white, sG.brounded, sG.ai_center, sG.jc_center]}>
                <View style={[sG.h_30, sG.w_85]}>
                  <Text style={[sG.h8, sG.text_primary]}>Name</Text>
                </View>
                <TextInput style={[sG.h_60, sG.w_85, sG.h7]} placeholder='Write your name' placeholderTextColor = '#BDBDBD'>
                </TextInput>
              </View>
            </View>
          </View>

          <View style={[sG.h_15, sG.w_100, sG.ai_center, sG.jc_center]}>
            <TouchableOpacity style={[sG.h_70, sG.w_90, sG.brounded, sG.bg_light, sG.ai_center, sG.jc_center, sG.bg_primary]} onPress={() =>  this.handlePress()}>
              <Text style={[sG.h6,sG.bold, sG.text_white]}>Next</Text>
            </TouchableOpacity>
          </View>

          <View style={[sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
          </View>
        </View>)}
      </View>
    );
  }
}

const stylesLocal = StyleSheet.create({
  
});

