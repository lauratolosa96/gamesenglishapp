import React, { Component } from "react";
import EStyleSheet from 'react-native-extended-stylesheet';
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialIcons, FontAwesome } from '@expo/vector-icons';


class Publication extends Component {
    render() {
        return (
            <View style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]}>
              <View style={[ sG.h_50, sG.w_100, sG.ai_center, sG.jc_center]}>
                <Image style={[ sG.w_100, sG.h_100, sG.brounded]} resizeMode='cover' source={require('../../../../assets/puzzle.jpg')}/>
              </View>
              <View style={[ sG.h_40, sG.w_100]}>
                <View style={[ sG.h_40, sG.w_100, sG.chrow]}>
                  <View style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]}>
                    <View style={[ sG.h_100, sG.w_100, sG.jc_center]}>
                      <Text style={[sG.h7, sG.bold, sG.text_primary]}>Título elemento</Text>
                    </View>
                  </View>
                </View>
                <View style={[ sG.h_60, sG.w_100, sG.jc_center]}>
                  <Text style={[sG.h8, sG.ai_center, sG.jc_center, sG.text_gray_light]}>
                Lorem ipsum dolor sit amet, consetetur{'\n'}sadipscing elitr, sed diam monumy eirmod tempor{'\n'}invidunt ut labore.
                </Text>
                </View>
              </View>
              <View style={[ sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
                <View style={[ sG.h_100, sG.w_100, sG.jc_center]}>
                  <Text style={[sG.h8, sG.bold, sG.text_primary]}>Ver más ></Text>
                </View>
              </View>
            </View>
        );
    }
}

 const stylesLocal = EStyleSheet.create({
    card_shadow: {
    borderWidth: 1,
    borderColor: '#bbbbbb',
    },
 })
export default Publication;