import React, { Component } from 'react';
import { Text, View, ScrollView, Image, TouchableOpacity, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { ListRegularVerbs } from '../../components/apis/apisDictionary/apiRegularVerbs';
import { ListIrregularVerbs } from '../../components/apis/apisDictionary/apiIrregularVerbs';
import { ListPronouns } from '../../components/apis/apisDictionary/apiPronounList';
import { ListPrepositionsOfPlace } from '../../components/apis/apisDictionary/apiPrepositionsOfPlaceList';
import { ListPrepositionsOfTime } from '../../components/apis/apisDictionary/apiPrepositionsOfTimeList';
import CardSearch from '../../components/dictionary/card_search';

export default class FolderDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      id_selected: 0,
      title_pag:'',
      line_one: '',
      line_two: '',
      line_three: '',
      line_four: '',
    };
  }

  verificar_api (operator) {
    if(operator == 0){
      this.setState({
        dataSource: ListRegularVerbs,
        line_one: 'Infinitive form: ',
        line_two: 'Past simple: ',
        line_three: 'Past Participle: ',
        line_four: 'Meaning: ',
      })
    }else if(operator == 1){
      this.setState({
        dataSource: ListIrregularVerbs,
        line_one: 'Infinitive form: ',
        line_two: 'Past simple: ',
        line_three: 'Past Participle: ',
        line_four: 'Meaning: ',
      })
    }else if(operator == 2){
      this.setState({
        dataSource: ListPronouns,
        line_one: 'Pronoun: ',
        line_two: 'Meaning: ',
        line_three: 'Example: ',
        line_four: 'Translation: ',
      })
    }else if(operator == 3){
      this.setState({
        dataSource: ListPrepositionsOfPlace,
        line_one: 'Preposition: ',
        line_two: 'Meaning: ',
        line_three: 'Example: ',
        line_four: 'Translation: ',
      })
    }else if(operator == 4){
      this.setState({
        dataSource: ListPrepositionsOfTime,
        line_one: 'Preposition: ',
        line_two: 'Meaning: ',
        line_three: 'Example: ',
        line_four: 'Translation: ',
      })
    }
  }

  
  componentDidMount() {
    const id_selected =  this.props.navigation.getParam('id')
    const title_pag =  this.props.navigation.getParam('title')
    const operator = id_selected-1
    return (
      this.setState({
        id_selected: id_selected-1,
        title_pag: title_pag,
      }),
      this.verificar_api(operator)
    );
  }
  //{this.state.dataSource[id].title}

  render() {
    const id = this.state.id_selected;
    return (
        <View style={[ sG.container, sG.bg_light]}>
            <View style={[ sG.w_100, sG.h_10, sG.ai_center, sG.jc_center]}>
              <Text style={[sG.h6, sG.bold, sG.text_primary]}>{this.state.title_pag}</Text>
            </View>
            
            <ScrollView>
            {this.state.dataSource.map((item, key) => (
              <View style={[ sG.row_40, sG.ai_center, sG.jc_cente]}>
                <View style={[ sG.h_100, sG.w_100, sG.ai_center, sG.jc_center]}>
                  <CardSearch title = {this.state.line_one+item.title} simple = {this.state.line_two+item.simple} participle = {this.state.line_three+item.participle} meaning = {this.state.line_four+item.meaning}/>
                </View>              
              </View>
            ))}
          </ScrollView>
      </View>
    );
  }
}