import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class PasswordThree extends Component {
  
  state = {
    checked: false,
    username: '',
    password: '',
    secureTextEntry: true ,
    iconName: "eye-off",
    }

  onIconPress = () => {
    let iconName = (this.state.secureTextEntry) ? "eye" : "eye-off";

    this.setState({
        secureTextEntry: !this.state.secureTextEntry,
        iconName : iconName,
        checked: !this.state.checked
    });
  }

  handlePress = () => {
    this.props.navigation.navigate('Login')
    Alert.alert(  
      'Proceso exitoso',  
      'Su contraseña ha sido recuperada',  
      [  
          {  
              text: 'Iniciar Sesión',  
              //onPress: () => this.props.navigation.navigate('Login'),  
              style: 'ok',  
          },   
      ],      
      {cancelable: false} 
    );  
  }

  render() {
    return (
      <View style={[ sG.container, sG.bg_light, sG.ai_center, sG.jc_center]}>

        <View style={[ sG.h_20, sG.w_100, sG.ai_center, sG.jc_end]}>
          <Text style={[sG.h8, sG.text_center, sG.text_gray_light]}>Ahora puedes cambiar la contraseña de tu cuenta,{'\n'}asegúrate de que puedes recordarla.
          </Text>
        </View>

        <View style={[ sG.h_5, sG.w_100, sG.ai_center, sG.jc_center]}></View>

        <View style={[sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
          <View style={[sG.chrow, sG.h_85, sG.w_90, sG.bg_white, sG.brounded, sG.ai_center, sG.jc_center]}>
            <View style={[sG.h_90, sG.w_70]}>
                <View style={[sG.h_30, sG.w_85]}>
                <Text style={[sG.h8, sG.text_primary]}>Nueva contraseña</Text>
              </View>
              <TextInput secureTextEntry={!this.state.checked} style={[sG.h_65, sG.w_100, sG.h7]} placeholder='***********' placeholderTextColor = '#BDBDBD'></TextInput>
            </View>
            <View style={[sG.h_90, sG.w_15,sG.ai_center, sG.jc_center]}>
              <MaterialCommunityIcons name={this.state.iconName} style={[sG.size_icon]} color='#177a76' checked={this.state.checked} onPress={this.onIconPress}/> 
            </View>
          </View>
        </View>

        <View style={[sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
          <View style={[sG.chrow, sG.h_85, sG.w_90, sG.bg_white, sG.brounded, sG.ai_center, sG.jc_center]}>
            <View style={[sG.h_90, sG.w_70]}>
                <View style={[sG.h_30, sG.w_85]}>
                <Text style={[sG.h8, sG.text_primary]}>Confirmar contraseña</Text>
              </View>
              <TextInput secureTextEntry={!this.state.checked} style={[sG.h_65, sG.w_100, sG.h7]} placeholder='***********' placeholderTextColor = '#BDBDBD'></TextInput>
            </View>
            <View style={[sG.h_90, sG.w_15,sG.ai_center, sG.jc_center]}>
              <MaterialCommunityIcons name={this.state.iconName} style={[sG.size_icon]} color='#177a76' checked={this.state.checked} onPress={this.onIconPress}/> 
            </View>
          </View>
        </View>

        <View style={[sG.h_5, sG.w_100, sG.ai_center, sG.jc_center]}>
          <View style={[sG.h_70, sG.w_90]}>
            <Text style={[sG.h8, sG.text_primary]}>Las contraseñas coinciden</Text>
          </View>
        </View>

        

        <View style={[sG.h_50, sG.w_100, sG.ai_center, sG.jc_end]}>
          <TouchableOpacity style={[sG.h_15, sG.w_100, sG.bg_light, sG.ai_center, sG.jc_center, sG.bg_primary]} onPress={() =>  this.handlePress()}>
            
            <Text style={[sG.h7, sG.text_white, sG.bold]}>Guardar contraseña</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
