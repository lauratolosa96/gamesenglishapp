import React from "react";
import { Text, Dimensions } from 'react-native';
import {
  createAppContainer,
  createBottomTabNavigator,
  createDrawerNavigator,
  createStackNavigator, 
  createSwitchNavigator
} from "react-navigation";
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Login from './src/screens/containers/begin/login';GamesDetails
import EStyleSheet from 'react-native-extended-stylesheet';
import RegisterOne from './src/screens/containers/begin/register1';
import RegisterTwo from './src/screens/containers/begin/register2';
import Home from './src/screens/containers/home/home';
import FolderList from './src/screens/containers/dictionary/foldersList';
import FolderDetails from './src/screens/containers/dictionary/folderDetails';
import GamesList from './src/screens/containers/games/gamesList';
import GamesDetails from './src/screens/containers/games/gamesDetails';
import AudioBooksList from './src/screens/containers/audioBooks/audioBooksList';
import AudioBooksDetails from './src/screens/containers/audioBooks/audioBooksDetails';
import ExamsList from './src/screens/containers/exams/examsList';
import ExamDetails from './src/screens/containers/exams/examDetails';
import { MaterialCommunityIcons, Ionicons, FontAwesome } from '@expo/vector-icons';

import Example from "./screens/Example";

const entireScreenWidth = Dimensions.get('window').width;

EStyleSheet.build({
  $rem: entireScreenWidth / 300,
  $colorPrimary: '#d81b60',
  $colorSecondary: '#e34747',
  $colorGrayLight: '#a0a0a0',
  $colorLight: '#feebee',
});

const Begin = createStackNavigator({
  // Landing: {
  //   screen: Login,
  //   navigationOptions: {
  //     headerTitle: "Landing"
  //   }
  // },
  Login: {
    screen: Login,
    navigationOptions: {
      header: null,
    }
  },
  RegisterOne: {
    screen: RegisterOne,
    navigationOptions: {
      header: null,
    }
  },
  RegisterTwo: {
    screen: RegisterTwo,
    navigationOptions: {
      header: null,
    }
  },
});

const HomeStack = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: {
      header: null,
    }
  },
  FolderList: {
    screen: FolderList,
    navigationOptions: {
      header: null,
    }
  },
  FolderDetails: {
    screen: FolderDetails,
    navigationOptions: {
      headerStyle:{ backgroundColor: '#d81b60'},
      headerTintColor: '#feebee',
    }
  },
});

const GamesStack = createStackNavigator({

  GamesList: {
    screen: GamesList,
    navigationOptions: {
      headerTitle: "Games List",
      headerTitleStyle: { 
        textAlign: 'center',
        flex:1
      },
      headerTransparent: {
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0
      },
      headerTintColor: '#fff',
    }
  },
  GamesDetails: {
    screen: GamesDetails,
    navigationOptions: {
      headerTitle: "",
      headerTitleStyle: { 
        textAlign: 'center',
        flex:1
      },
      headerTransparent: {
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0
      },
      headerTintColor: '#fff',
    }
  },
});

const AudioBooksStack = createStackNavigator({
  AudioBooksList: {
    screen: AudioBooksList,
    navigationOptions: {
      headerTitle: "Audiobooks List",
      headerTitleStyle: { 
        textAlign: 'center',
        flex:1
      },
      headerTransparent: {
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0
      },
      headerTintColor: '#fff',
    }
  },
  AudioBooksDetails: {
    screen: AudioBooksDetails,
    navigationOptions: {
      headerTitle: "",
      headerTitleStyle: { 
        textAlign: 'center',
        flex:1
      },
      headerTransparent: {
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0
      },
      headerTintColor: '#fff',
    }
  },
});

const ExamsStack = createStackNavigator({
  ExamsList: {
    screen: ExamsList,
    navigationOptions: {
      headerTitle: "Exams List",
      headerTitleStyle: { 
        textAlign: 'center',
        flex:1
      },
      headerTransparent: {
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0
      },
      headerTintColor: '#fff',
    }
  },
  ExamDetails: {
    screen: ExamDetails,
    navigationOptions: {
      headerTitle: "",
      headerTitleStyle: { 
        textAlign: 'center',
        flex:1
      },
      headerTransparent: {
        position: 'absolute',
        backgroundColor: 'transparent',
        zIndex: 100,
        top: 0,
        left: 0,
        right: 0
      },
      headerTintColor: '#fff',
    }
  },
});

const MainTabs = createMaterialBottomTabNavigator({
  Home: {
    screen: HomeStack,
    navigationOptions: {
      tabBarLabel: "Home",
      tabBarIcon: ({ focused }) => (<MaterialCommunityIcons name='home' size={25} style={ focused ? {color: '#feebee'} : {color: '#a0a0a0'}}/>),
      //activeTintColor: '#177a76',
    }
  },
  GamesStack: {
    screen: GamesStack,
    navigationOptions: {
      tabBarLabel: "Games",
      tabBarIcon: ({ focused }) => (<FontAwesome name='gamepad' size={20} style={ focused ? {color: '#feebee'} : {color: '#a0a0a0'}}/>),
      //activeTintColor: '#177a76',
    }
  },
  AudioBooksStack: {
    screen: AudioBooksStack,
    navigationOptions: {
      tabBarLabel: "Audiobooks",
      tabBarIcon: ({ focused }) => (<MaterialCommunityIcons name='audiobook' size={25} style={ focused ? {color: '#feebee'} : {color: '#a0a0a0'}}/>),
      //activeTintColor: '#177a76',
    }
  },
  ExamsStack: {
    screen: ExamsStack,
    navigationOptions: {
      tabBarLabel: "Exams",
      tabBarIcon: ({ focused }) => (<Ionicons name='ios-paper' size={25} style={ focused ? {color: '#feebee'} : {color: '#a0a0a0'}}/>),
      //activeTintColor: '#177a76',
    }
  },
   
},
  {  
    initialRouteName: "Home",  
    activeColor: '#feebee',  
    inactiveColor: '#a0a0a0',  
    barStyle: { backgroundColor: '#d81b60' },  
  }
);

const SettingsStack = createStackNavigator({
  SettingsList: {
    screen: Example,
    navigationOptions: {
      headerTitle: "Settings List"
    }
  },
});

const AppModalStack = createStackNavigator(
  {
    MainTabs: MainTabs,
    Settings: SettingsStack,
    Promotion1: {
      screen: Example
    }
  },
  {
    mode: "modal",
    headerMode: "none"
  }
);

const App = createSwitchNavigator({
  Auth: {
    screen: Begin
    //screen: Home,
  },
  App: {
    screen: AppModalStack
  }
});

export default createAppContainer(App);