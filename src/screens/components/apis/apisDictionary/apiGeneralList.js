const List =[
    {
        "id": 1,
        "nombre": "Regular Verbs",
    },
    {
        "id": 2,
        "nombre": "Irregular verbs",
    },
    {
        "id": 3,
        "nombre": "Personal Pronouns",
    },
    {
        "id": 4,
        "nombre": "Prepositions of Place",
    },
    {
        "id": 5,
        "nombre": "Prepositions of Time",
    },
]

export { List }