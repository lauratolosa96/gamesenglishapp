import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity, ImageBackground } from 'react-native';
import {sG} from '../../components/general/styles';
import { MaterialCommunityIcons } from '@expo/vector-icons';

export default class RegisterOne extends Component {
  
  state = {
    checked: false,
    username: '',
    password: '',
    secureTextEntry: true ,
    iconName: "eye-off",
    }

    handlePress = () => {
      this.props.navigation.navigate('RegisterTwo')
    }

  onIconPress = () => {
    let iconName = (this.state.secureTextEntry) ? "eye" : "eye-off";

    this.setState({
        secureTextEntry: !this.state.secureTextEntry,
        iconName : iconName,
        checked: !this.state.checked
    });
  }

  render() {
    return (
      <View style={[ sG.container, sG.bg_light]}>
        <ImageBackground style={[sG.w_100,sG.container, sG.ai_center, sG.jc_center]}
      resizeMode='cover' source={require('../../../../assets/fondo1.png')}>

            <View style={[sG.h_10, sG.w_100, sG.ai_center, sG.jc_center]}>
              <Text style={[sG.h5, sG.bold, sG.text_primary]}>Support team</Text>
            </View>

            <View style={[sG.h_50, sG.w_100, sG.ai_center, sG.jc_center]}>
              <View style={[sG.h_50, sG.w_100, sG.ai_center, sG.jc_center]}>
                <View style={[sG.h_80, sG.w_90, sG.brounded, sG.card_shadow, sG.ai_center, sG.jc_center, sG.bg_white]}>
                  <View style={[sG.h_90, sG.w_90, sG.ai_center, sG.jc_center]}>
                    <Text style={[sG.h7, sG.text_center, sG.ai_center, sG.jc_center, sG.text_black]}>
                    ¡Bienvenido!, somos el equipo de apoyo, estaremos para ayudarte en tu proceso de aprendizaje, ¡adelante!
                    </Text>
                  </View>
                </View>
              </View>
              <View style={[sG.h_50, sG.w_100, sG.ai_center, sG.jc_center]}>
                <View style={[sG.h_80, sG.w_90, sG.brounded, sG.card_shadow, sG.ai_center, sG.jc_center, sG.bg_white]}>
                  <View style={[sG.h_90, sG.w_90, sG.ai_center, sG.jc_center]}>
                    <Text style={[sG.h7, sG.text_center, sG.ai_center, sG.jc_center, sG.text_black]}>
                    Welcome !, we are the help team, we will be there to help you in your learning process, go ahead!
                    </Text>
                  </View>
                </View>
              </View>
            </View>

            <View style={[stylesLocal.h_body, sG.chrow, sG.w_100, sG.ai_center, sG.jc_center]}>
              <View style={[ sG.h_100, sG.w_50, sG.ai_center, sG.jc_center]}>
                <Image style={[ sG.w_60, sG.h_90, sG.brounded]} resizeMode='cover' source={require('../../../../assets/help1.png')}/>
              </View>
              <View style={[ sG.h_100, sG.w_50, sG.ai_center, sG.jc_center]}>
                <Image style={[ sG.w_60, sG.h_90, sG.brounded]} resizeMode='cover' source={require('../../../../assets/help2.png')}/>
              </View>
            </View>

            <View style={[stylesLocal.bc_footer_botton, sG.w_100, sG.ai_center, sG.jc_center]}>
              <TouchableOpacity style={[sG.h_100, sG.w_100, sG.ai_center, sG.jc_center, sG.bg_primary]} onPress={() =>  this.handlePress()}>
                <Text style={[sG.h7, sG.text_white, sG.bold]}>Next</Text>
              </TouchableOpacity>
            </View>
        </ImageBackground>
      </View>
    );
  }
}

const stylesLocal = StyleSheet.create({
  bc_botton:{
    height:'7%'
  },
  h_body:{
    height:'32%'
  },
  bc_footer_botton:{
    height:'8%'
  },
});
